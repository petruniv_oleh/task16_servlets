package com.oleh.model.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.oleh.model.Pizza;


public class GsonParser {

    private Gson gson;
    private Pizza pizza;
    private String json;

    public GsonParser(String json) {
        gson = new GsonBuilder().create();
        this.json = json;
        parse();
    }

    private void parse() {
        pizza = gson.fromJson(json, Pizza.class);
        JsonElement jsonElement = gson.toJsonTree(json);


    }

    public Pizza getPizza() {
        return pizza;
    }


}
