package com.oleh.model;


public class Pizza {
    private static int orderNumber = 0;
    private int diameter;
    private String name;
    private int price;
    private String ingredients;


    public Pizza() {
    }

    public Pizza(int diameter, String name, int price, String ingredients) {
        this.diameter = diameter;
        this.name = name;
        this.price = price;
        this.ingredients = ingredients;
        orderNumber++;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }
}
