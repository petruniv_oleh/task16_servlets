package com.oleh;

import com.oleh.model.Pizza;
import com.oleh.model.parser.GsonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@WebServlet(urlPatterns = "/orders/*",
        name = "PizzaServlet",
        displayName = "My Servlet")
public class PizzaServlet extends HttpServlet {

    private Map<Integer, Pizza> pizzas = new LinkedHashMap<Integer, Pizza>();
    private static Logger logger = LogManager.getLogger(PizzaServlet.class.getName());


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Invoke doGet()");
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.println("<html><head></head><body>");
        writer.println("<h1>Orders</h1><hr>");
        if (pizzas.size() > 0) {
            logger.debug("out pizzas");
            Set<Integer> integers = pizzas.keySet();
            for (int key : integers
            ) {
                Pizza pizza = pizzas.get(key);
                writer.println("<h3>Order number: " + key + "</h3>");
                writer.println("<p>Name: " + pizza.getName() + "</p>" +
                        "<p>Diameter "
                        + pizza.getDiameter() + " </p>" +
                        "<p>Price: " + pizza.getPrice() + "</p>");
                writer.println("<p>Ingredients: " + pizza.getIngredients() + "</p><hr>");

            }
        } else {
            logger.debug("no pizzas");
            writer.println("<p>There is no order yet</p>");
        }

        writer.println("<h3>Add pizza</h3>");
        writer.println("<form actions='orders' method='POST'>" +
                "<p>Pizza name: </p><input type='text' name='pizza_name'/>" +
                "<p>Size: </p><input type='text' name='pizza_diametr'/>" +
                "<p>Price: </p><input type='text' name='pizza_price'/>" +
                "<p>Ingredients: </p><input type='text' name='pizza_ingredients'/><br><br>" +
                "<input type='submit'/><br>" +
                "</form>"
        );
        writer.println("<form>" +
                "<p>Delete order</p>" +
                "<p>Order number</p>" +
                "<input type='text' name = 'order_number'><br><br>" +
                "<input type='button' onclick='remove(order_number.value)' name='delete_btn' value='delete'>" +
                "</form>");
        writer.println("<h3>Change order data</h3>");
        writer.println("<form>" +
                "<p>Order number</p>" +
                "<input type='text' name = 'order_number' id='number'><br>" +
                "<p>Pizza name: </p><input type='text' name='pizza_name' id='p-name' value='0'/>" +
                "<p>Size: </p><input type='text' name='pizza_diameter' id='diameter' value='0'/>" +
                "<p>Price: </p><input type='text' name='pizza_price' id ='price' value='0'/>" +
                "<p>Ingredients: </p><input type='text' name='pizza_ingredients' id='ingredients' value='0'/><br><br>" +
                "<input type='button' onclick='change(order_number.value)' name='change_btn' value='change'/><br>" +
                "</form>"
        );
        writer.println("<script type='text/javascript'>" +
                "function remove(order_number){\n" +
                "fetch('orders/'+order_number, {method: 'delete'});\n}" +
                "function change(order_number){\n" +
                "var data = {" +
                "orderNumber : document.getElementById('number').value," +
                "diameter : document.getElementById('diameter').value," +
                "name : document.getElementById('p-name').value," +
                "price : document.getElementById('price').value," +
                "ingredients : document.getElementById('ingredients').value" +
                "};" +
                "fetch('orders/'+order_number,{" +
                "method: 'PUT'," +
                "headers: {\n" +
                "            'Content-Type': 'application/json',\n" +
                "        }," +
                "body: JSON.stringify(data)" +
                "});" +
                "}" +
                "</script>");
        writer.println("</html></body>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("invoke doPost()");
        String name = req.getParameter("pizza_name");
        String diameter = req.getParameter("pizza_diametr");
        String price = req.getParameter("pizza_price");
        String ingredients = req.getParameter("pizza_ingredients");
        Pizza pizza = new Pizza(Integer.parseInt(diameter), name, Integer.parseInt(price), ingredients);

        pizzas.put(pizza.getOrderNumber(), pizza);
        logger.debug("add pizza");
        doGet(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Invoke doDelete");
        String requestURI = req.getRequestURI();
        requestURI = requestURI.replace("/task16_servlets_war_exploded/orders/", "");
        Pizza pizza = pizzas.get(Integer.parseInt(requestURI));

        BufferedReader reader = req.getReader();
        String line = reader.readLine();
        GsonParser gParser = new GsonParser(line);
        Pizza pizzaChanges = gParser.getPizza();
        String name = pizzaChanges.getName();
        if (!name.isEmpty()&&!name.equals("0")){
            pizza.setName(name);
        }

        int diameter = pizzaChanges.getDiameter();
        if (diameter!=0){
            pizza.setDiameter(diameter);
        }

        int price = pizzaChanges.getPrice();
        if (price!=0){
            pizza.setPrice(price);
        }

        String ingredients = pizzaChanges.getIngredients();
        if (!ingredients.isEmpty()&&!ingredients.equals("0")){
            pizza.setIngredients(ingredients);
        }

        pizzas.remove(Integer.parseInt(requestURI));
        pizzas.put(Integer.parseInt(requestURI), pizza);
        super.doPut(req, resp);
    }


    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("Invoke doDelete");
        String requestURI = req.getRequestURI();
        requestURI = requestURI.replace("/task16_servlets_war_exploded/orders/", "");
        logger.debug("delete order number " + requestURI);
        pizzas.remove(Integer.parseInt(requestURI));
    }
}
